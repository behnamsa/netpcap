from packet_serialize import *
from datetime import datetime
from time import time

from socket import *
import argparse

class PcapWriter:
    def __init__(self, filename):
        self.file = open(filename, 'wb')
        self.file.write(pack('IHHiIII', 0xa1b2c3d4, 2, 4, 0, 0, 65535, 1))

    def write(self, data):
        now = time()
        self.file.write(pack('IIII', int(now), int((now % 1) * 10**6), len(data), len(data)) + data)
        
    def close(self):
        self.file.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Capture ip packets and store them in a pcap file')
    parser.add_argument('-o', '--output', help='the output file name', required=True)
    args = parser.parse_args()

    sock = socket(AF_PACKET, SOCK_RAW, ntohs(3))
    writer = PcapWriter(args.output)
    count = 0

    try:
        while True:
            data, address = sock.recvfrom(65535)
            writer.write(data)

            count += 1
            print('Packet #' + str(count), 'from interface', address[0], 'at', datetime.now())

            packet = extract_frame(data)
            print(textwrap.indent(str(packet), '    '))

    except KeyboardInterrupt:
        print('Exiting')
    finally:
        sock.close()
        writer.close()
