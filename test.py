import unittest
from packet_serialize import *

class PacketConstructTestCase(unittest.TestCase):
    def test_ethernet(self):
        obj = ethernet_frame(1112, 1113, b'232111', 0)
        rec = ethernet_frame.extract(obj.serialize())
        self.assertEqual(str(obj), str(rec))

    def test_ip(self):
        obj = ip_datagram(1112, 1113, b'232111', 0)
        rec = ip_datagram.extract(obj.serialize())
        self.assertEqual(str(obj), str(rec))

    def test_icmp(self):
        obj = icmp_message(12, 13, b'1234', b'abcd')
        rec = icmp_message_base.extract(obj.serialize())
        self.assertEqual(str(obj), str(rec))

        obj = icmp_echo_message(1234, 4321, b'1234')
        rec = icmp_message_base.extract(obj.serialize())
        self.assertEqual(str(obj), str(rec))

        rep = icmp_echo_message(1234, 4321, b'1234', reply=True)
        self.assertNotEqual(str(obj), str(rep))

    def test_arp(self):
        obj = arp_datagram(12, 1234, 0xab, 0xabcd, True)
        rec = arp_datagram.extract(obj.serialize())
        self.assertEqual(str(obj), str(rec))

    def test_tcp(self):
        obj = tcp_segment(1234, 0xabcd, 12, 0xab, 12, 13, TcpFlags.SYN, b'hi!')
        rec = tcp_segment.extract(obj.serialize())
        self.assertEqual(str(obj), str(rec))

    def test_udp(self):
        obj = udp_segment(1234, 0xabcd, 12, 0xab, b'hi!')
        rec = udp_segment.extract(obj.serialize())
        self.assertEqual(str(obj), str(rec))

    def test_dns(self):
        obj = dns_message(1234, DnsHeaderFlags.QR | DnsHeaderFlags.AA | DnsHeaderFlags.NOTZONE,
                          questions=[DnsQuestionEntry(DnsDomainName('abcd.com'), DnsRRType.A, DnsRRClass.IN)],
                          answers=[DnsRREntry(DnsDomainName('a.b.c.d'), DnsRRType.CNAME, DnsRRClass.HS, 0xabcd, b'xy')])
        rec = dns_message.extract(obj.serialize())
        self.assertEqual(str(obj), str(rec))

if __name__ == '__main__':
    unittest.main()
