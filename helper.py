from struct import *
from enum import IntFlag, IntEnum
import textwrap

def mac_to_str(addr):
    parts = unpack('!6B', int.to_bytes(addr, 6, 'big'))
    return ':'.join(['{:02X}'] * 6).format(*parts)


def ip_to_str(addr):
    parts = unpack('!4B', int.to_bytes(addr, 4, 'big'))
    return '.'.join(['{}'] * 4).format(*parts)


def ip_flags_to_str(number):
    flags = []
    if number & 2: flags.append("Don't Fragment")
    if number & 1: flags.append('More Fragments')
    return ', '.join(flags) if flags else 'None'


def calculate_checksum(buffer):
    if len(buffer) % 2 == 1:
        buffer += b'\x00'

    shorts = unpack(f'!{len(buffer) // 2}H', buffer)

    sum = 0
    for short in shorts:
        sum = sum + short
        sum = sum % 0x10000 + sum // 0x10000
    return 0x10000 - sum - 1  # one's complement


def format_lines(strings, indent=4, *args, **kwargs):
    string = ''.join([s.replace('\t', indent * ' ') + '\n' for s in strings])
    return string.format(*args, **kwargs)


def format_object(name, payload, width=70, indent=4):
    if type(payload) not in (bytes, str, list):
        # payload is a decoded packet
        return str(payload)

    # pyload is empty
    if len(payload) == 0:
        return ' ' * indent + name + ': None\n'

    if type(payload) in (str, bytes):
        payload = [payload]

    string = ' ' * indent + name + ':\n'
    for item in payload:
        if type(item) is str:
            parts = [line[i:i+width] for line in item.splitlines() for i in range(0, len(line), width)]
            string += ''.join(map(lambda s: ' ' * (indent + 4) + s + '\n', parts))
        elif type(item) is bytes:
            item = str(item)
            parts = [item[i:i+width] for i in range(0, len(item), width)]
            string += ''.join(map(lambda s: ' ' * (indent + 4) + s + '\n', parts))
        else:
            string += ' ' * (indent + 4) + str(item) + '\n'
    return string


class NetFlag(IntFlag):
    def __str__(self):
        names = [member.name for member in type(self) if member in self]
        return ' '.join(names)

    def __format__(self, format_spec):
        return format(str(self), format_spec)


class TcpFlags(NetFlag):
    FIN = 0x01
    SYN = 0x02
    RST = 0x04
    PSH = 0x08
    ACK = 0x10
    URG = 0x20
    ECE = 0x40
    CWR = 0x80
    NS = 0x100


class DnsHeaderFlags(NetFlag):
    #  = value , mask
    QR = 0x8000, 0x8000

    # OpCode
    QUERY  = 0 * 0x800, 0x7800
    IQUERY = 1 * 0x800, 0x7800
    STATUS = 2 * 0x800, 0x7800
    NOTIFY = 4 * 0x800, 0x7800
    UPDATE = 5 * 0x800, 0x7800
    DSO    = 6 * 0x800, 0x7800

    AA = 0x400, 0x400
    TC = 0x200, 0x200
    RD = 0x100, 0x100
    RA = 0x080, 0x080
    AD = 0x020, 0x020
    CD = 0x010, 0x010

    #RCode
    NOERROR   =  0, 0x0f
    FORMERR   =  1, 0x0f
    SERVFAIL  =  2, 0x0f
    NXDOMAIN  =  3, 0x0f
    NOTIMP    =  4, 0x0f
    REFUSED   =  5, 0x0f
    YXDOMAIN  =  6, 0x0f
    YXRRSET   =  7, 0x0f
    NXRRSET   =  8, 0x0f
    NOTAUTH   =  9, 0x0f
    NOTZONE   = 10, 0x0f
    DSOTYPENI = 11, 0x0f
    BADVERS   = 16, 0x0f
    BADSIG    = 16, 0x0f
    BADKEY    = 17, 0x0f
    BADTIME   = 18, 0x0f
    BADMODE   = 19, 0x0f
    BADNAME   = 20, 0x0f
    BADALG    = 21, 0x0f
    BADTRUNC  = 22, 0x0f
    BADCOOKIE = 23, 0x0f

    def __new__(cls, value, mask):
        obj = IntFlag.__new__(cls, value)
        obj._value_ = value
        obj.mask = mask
        return obj

    def __contains__(self, item):
        return item.value == self.value & item.mask


class DnsRRType(NetFlag):
    A = 1
    NS = 2
    MD = 3
    MF = 4
    CNAME = 5
    SOA = 6
    MB = 7
    MG = 8
    MR = 9
    NULL = 10
    WKS = 11
    PTR = 12
    HINFO = 13
    MINFO = 14
    MX = 15
    TXT = 16
    RP = 17
    AFSDB = 18
    X25 = 19
    ISDN = 20
    RT = 21
    NSAP = 22
    NSAP_PTR = 23
    SIG = 24
    KEY = 25
    PX = 26
    GPOS = 27
    AAAA = 28
    LOC = 29
    NXT = 30
    EID = 31
    NIMLOC = 32
    SRV = 33
    ATMA = 34
    NAPTR = 35
    KX = 36
    CERT = 37
    A6 = 38
    DNAME = 39
    SINK = 40
    OPT = 41
    APL = 42
    DS = 43
    SSHFP = 44
    IPSECKEY = 45
    RRSIG = 46
    NSEC = 47
    DNSKEY = 48
    DHCID = 49
    NSEC3 = 50
    NSEC3PARAM = 51
    TLSA = 52
    SMIMEA = 53
    HIP = 55
    NINFO = 56
    RKEY = 57
    TALINK = 58
    CDS = 59
    CDNSKEY = 60
    OPENPGPKEY = 61
    CSYNC = 62
    ZONEMD = 63
    SVCB = 64
    HTTPS = 65
    SPF = 99
    UINFO = 100
    UID = 101
    GID = 102
    UNSPEC = 103
    NID = 104
    L32 = 105
    L64 = 106
    LP = 107
    EUI48 = 108
    EUI64 = 109
    TKEY = 249
    TSIG = 250
    IXFR = 251
    AXFR = 252
    MAILB = 253
    MAILA = 254
    ALL = 255
    URI = 256
    CAA = 257
    AVC = 258
    DOA = 259
    AMTRELAY = 260

    def __contains__(self, item):
        return item.value == self.value


class DnsRRClass(NetFlag):
    IN = 1
    CS = 2
    CH = 3
    HS = 4
    NONE = 254
    ANY = 255

    def __contains__(self, item):
        return item.value == self.value


class DnsDomainName:
    def __init__(self, name):
        self.string = name

    @classmethod
    def extract(cls, buffer, offset=0):
        labels = []

        def recursive(offset):
            length = buffer[offset]
            offset += 1
            if length == 0:
                return offset
            if length < 0b0100_0000:
                labels.append(buffer[offset:offset + length].decode('utf-8', 'backslashreplace').replace('.', '\\.'))
                return recursive(offset + length)
            elif length < 0b1100_0000:
                raise ValueError()
            else:
                offset += 1
                pointer = int.from_bytes(buffer[offset-2:offset], 'big') & 0x3fff
                recursive(pointer)
                return offset

        offset = recursive(offset)
        return DnsDomainName('.'.join(labels)), offset

    def serialize(self):
        parts = self.string.replace('\\.', '\0').split('.')
        buffer = b''
        for part in parts:
            buffer += len(part).to_bytes(1, 'big') + part.replace('\0', '.').encode('utf-8')
        return buffer + b'\x00'

    def __str__(self):
        return self.string


class DnsQuestionEntry:
    def __init__(self, name: DnsDomainName, type, cls):
        self.name = name
        self.type = DnsRRType(type)
        self.cls = DnsRRClass(cls)

    @classmethod
    def extract(cls, buffer, offset):
        name, offset = DnsDomainName.extract(buffer, offset)
        type, cls_ = unpack_from('!HH', buffer, offset)
        return DnsQuestionEntry(name, type, cls_), offset + 4

    def serialize(self):
        return self.name.serialize() + pack('!HH', self.type, self.cls)

    def __str__(self):
        return f'{self.name} {self.type.name} {self.cls.name}'

class DnsRREntry:
    def __init__(self, name: DnsDomainName, type, cls, ttl, rdata=b'', rdlength=None):
        self.name = name
        self.type = DnsRRType(type)
        self.cls = DnsRRClass(cls)
        self.ttl = ttl
        self.rdata = rdata
        self.rdlength = len(rdata) if rdlength is None else rdlength

    @classmethod
    def extract(cls, buffer, offset):
        name, offset = DnsDomainName.extract(buffer, offset)
        type, cls_, ttl, rdlength = unpack_from('!HHiH', buffer, offset)
        rdata = buffer[offset + 10:offset + 10 + rdlength]
        return DnsRREntry(name, type, cls_, ttl, rdata, rdlength), offset + 10 + rdlength

    def serialize(self):
        return self.name.serialize() + pack('!HHiH', self.type, self.cls, self.ttl, self.rdlength) + self.rdata

    def __str__(self):
        return f'{self.type.name} {self.cls.name} {self.name} ttl={self.ttl} rdata={self.rdata}'

