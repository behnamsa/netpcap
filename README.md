# netpcap
A simple packet capture program written in python

Needs at least python 3.6, no dependencies required

#### Sample usage
```shell script
$ sudo python3 main.py --output capture.pcap
```
